import React, { PureComponent } from 'react';
import Header from '../../shared/components/Header';
import { Jumbotron } from 'reactstrap';

class HomePage extends PureComponent {
  render() {
    return (
      <div className='home-page'>
        <Header/>

        <div>
          <Jumbotron>
            <h1 className="display-3">Hello, world and OTS! =)</h1>
          </Jumbotron>
        </div>
      </div>
    );
  }
}

export default HomePage;
