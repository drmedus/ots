import React, { PureComponent } from 'react';
import Header from '../../shared/components/Header';
import ActorsList from "./ActorsList/ActorsList";

class ActorsPage extends PureComponent {
  render() {
    return (
      <React.Fragment>
        <Header/>
        <ActorsList/>
      </React.Fragment>
    );
  }
}

export default ActorsPage;
