import React, { PureComponent } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class NewActorForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      actor: '',
    };
  }

  addActor = () => {
    this.props.addActorHandler(this.state.actor);
    this.setState({ actor: '' });
  }

  handleChange = (event) => {
    this.setState({ actor: event.target.value });
  }

  render() {
    return (
      <React.Fragment>
        <Form >
          <FormGroup>
            <Label for="actor-name">New actor</Label>
            <Input type="text" name="actor-name" id="actor-name" value={this.state.actor} onChange={this.handleChange} placeholder="Name"/>
          </FormGroup>
          <Button onClick={this.addActor}>Add</Button>
        </Form>
      </React.Fragment>
    );
  }
}

export default NewActorForm;
