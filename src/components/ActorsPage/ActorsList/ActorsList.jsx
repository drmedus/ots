import React, { Component } from 'react';
import { getActorsNames } from "../../../shared/apiMethods/actors";
import { ListGroup, ListGroupItem } from 'reactstrap';
import NewActorForm from "./NewActorForm/NewActorForm";

class ActorsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      actors: [],
    };
  }

  addActorHandler = (actorName) => {
    this.setState((prevState) => {
      let currentList = prevState.actors;
      currentList.push(actorName);
      return { actors: currentList }
    });
  }

  removeActorHandler = (index) => {
    this.setState((prevState) => {
      let currentList = prevState.actors;
      currentList.splice(index, 1);
      return { actors: currentList }
    });
  }

  componentDidMount() {
    getActorsNames().then(data => this.setState((prevState) => {
      return { actors: [...prevState.actors, ...data] }
      // Мерж, на случай если мы успели добавить актера с клавиатуры раньше, чем получили респонс от апи.
      // Либо прелоадер и отсуствие формы ввода до получения ответа, чтобы не перетереть данные стейта.
    }));
  }

  render() {
    return (
      <React.Fragment>
        <span>Сlick on the actor to remove</span>
        <ListGroup>
          {
            this.state.actors.map(
              (actor, index) => (<ListGroupItem key={index} onClick={this.removeActorHandler.bind(this, index)}>{actor}</ListGroupItem>)
              // небольшой финт ушами с .bind чтобы упростить архитектуру при текущем функционале и не
              // делать отдельный компонент для ListGroupItem. При усложнении логики, это будет, конечно же,
              // отдельным компонентом, в который и перенесется удаление
            )
          }
        </ListGroup>
        <NewActorForm addActorHandler={this.addActorHandler} />
      </React.Fragment>
    );
  }
}

export default ActorsList;
