import axios from 'axios';
import { API_URL } from '../../config';

let getActors = async () => {
  let res = await axios.get(API_URL);
  return await res.data.results;
};

let getActorsNames = async () => {
  return await getActors().then(data => {

    let actorsNames = data.map((item) => {
      return item.name;
    });

    return actorsNames;
    }
  );
};

export { getActors, getActorsNames };
